<?php
/**
 * Created by JetBrains PhpStorm.
 * User: xjw129xjt
 * Date: 13-7-24
 * Time: 下午6:36
 * To change this template use File | Settings | File Templates.
 */

class CompetitionAction extends Action{

    public function index()
    {
        $user = D('User')->getUserInfo($this->mid);
        $this->assign('uid', $user['uid']);
        $this->assign('uname', $user['uname']);
        $condition['uid'] = $this->mid;
        $info = D('bitcoin_com_info')->where($condition)->select();
        if($info == null){
            $info = D('bitcoin_com_info');
            $data['dollar']=500000;
            $data['uid']= $this->mid;
            $data['uname']= $user['uname'];
            $data['btc']=0;
            $data['f_dollar']=0;
            $data['f_btc']=0;
            //  dump($data);exit;
            $info->data($data)->add();
        }

        $this->display();

    }

    public function buy()
    {
        $usd=S('usd');
        $usd1=number_format($usd['return']['last_all']['value'],2);
        $this->assign('usd_value', $usd1);
        $this->assign('usd', S('usd'));
        // dump($usd);exit;
        $this->display();
    }

    public function sell()
    {
        $usd=S('usd');
        $usd1=number_format($usd['return']['last_all']['value'],2);
        $this->assign('usd_value', $usd1);
        $this->assign('usd', S('usd'));
        //dump($usd);exit;
        $this->display();
    }


    public function  myentrust()
    {
        $this->display();
    }

    public function  myturnover()
    {
        $this->display();
    }


    public function market()
    {
        $this->display();
    }
    /*排行榜*/
    public function ranking_list()
    {
        $this->display();
    }


//交易，买入交易和卖出交易
    function deal()
    {
        $usd=S('usd');
        $usd1=number_format($usd['return']['last_all']['value'],2);
        $entrust = D('bitcoin_com_entrust');
        $data['uid']=$this->mid;
        $data['type'] = $_POST["type"];
        $data['price'] = $_POST["price"];
        $data['number'] = $_POST["number"];
        if($usd1 == 0){
            $data['state'] = 0;
            $data['entrust_time']=time();
        }
        if (($data['type'] == 0 )&&($usd1 != 0)) {
            if ($data['price'] >= $usd1) {
                $data['state'] = 1;
                $data['turnover_time']=time();
                $data['entrust_time']=$data['turnover_time'];
            } elseif ($data['price'] <$usd1) {
                $data['state'] = 0;
                $data['entrust_time']=time();

            }
        }
        if ($data['type'] == 1) {
            if ($data['price'] >$usd1) {
                $data['state'] = 0;
                $data['entrust_time']=time();
            } elseif ($data['price'] <= $usd1) {
                $data['state'] = 1;
                $data['turnover_time']=time();
                $data['entrust_time']=$data['turnover_time'];
            }
        }


        // M('BitcoinInfoWidget')->update_info($data);
        $result= $this->update_info($data);

        if($result == 1){
            $entrust->data($data)->add();
            $this->success("");
        }
        else{$this->error("");}
    }




    public function update_info($entrust)
    {

        $update_info = D('bitcoin_com_info');
        $condition['uid'] = $this->mid;
        $info = D('bitcoin_com_info')->where($condition)->select();
        $entrust['sum'] = $entrust['price'] * $entrust['number'];
        if ($entrust['type'] == 0) {
            if ($entrust['state'] == 0) {
                $data['dollar'] = $info['0']['dollar'] - $entrust['sum'];
                $data['f_dollar'] = $info['0']['f_dollar'] + $entrust['sum'];
                if( $data['dollar']>=0 &&  $data['f_dollar']>=0){
                    $update_info->where($condition)->save($data);
                    return '1';
                }
                else{ return '0'; }
            } elseif ($entrust['state'] == 1) {
                $data['dollar'] = $info['0']['dollar'] - $entrust['sum'];
                $data['btc'] = $info['0']['btc'] + $entrust['number'];
                if($data['dollar'] >= 0 &&  $data['btc'] >= 0){
                    $update_info->where($condition)->save($data);
                    return '1';
                }
                else{return '0';}
            }
        }

        elseif ($entrust['type'] == 1) {
            if ($entrust['state'] == 0) {
                $data['btc'] = $info['0']['btc'] - $entrust['number'];
                $data['f_btc'] = $info['0']['f_btc'] + $entrust['number'];
                if($data['btc'] >= 0 &&  $data['f_btc'] >= 0){
                    $update_info->where($condition)->save($data);
                    return '1';
                }
                else{ return '0'; }
            } elseif ($entrust['state'] == 1) {
                $data['btc'] = $info['0']['btc'] - $entrust['number'];
                $data['dollar'] = $info['0']['dollar'] + $entrust['sum'];
                if( $data['btc'] >= 0 && $data['dollar'] >= 0){
                    $update_info->where($condition)->save($data);
                    return '1';
                }
                else{ return '0'; }
            }
        }
    }


   public function delete_entrust(){
       $id= $_POST["id"];
       $condition1['uid']=$this->mid;
       $info = D('bitcoin_com_info')->where($condition1)->select();

       $update_info = D('bitcoin_com_info');
       $condition['id'] =$id ;
       $myentrust=D('bitcoin_com_entrust')->where($condition)->select();
         if($myentrust['0']['type']==0){
            $data['dollar']=$info['0']['dollar']+$myentrust['0']['price']*$myentrust['0']['number'];
            $data['f_dollar']=$info['0']['f_dollar']-$myentrust['0']['price']*$myentrust['0']['number'];
             $update_info->where($condition1) ->save($data);
       }
       if($myentrust['0']['type']==1){
           $data['btc']=$info['0']['btc']+$myentrust['0']['number'];
           $data['f_btc']=$info['0']['f_btc']-$myentrust['0']['number'];
           $update_info->where($condition1) ->save($data);
       }


       $entrust = D('bitcoin_com_entrust');
       $entrust->where($condition)->delete();

   }



    public function closeposition(){

        $id= $_POST["id"];
        $condition1['uid']=$this->mid;
        $info = D('bitcoin_info')->where($condition1)->select();
        $update_info = D('bitcoin_info');
        $usd=S('usd');
        $usd1=number_format($usd['return']['last_all']['value'],2);
        $condition['id'] =$id ;
        $entrust = D('bitcoin_entrust');
        $myentrust=D('bitcoin_entrust')->where($condition)->select();
        if($usd1!=0){
            if($myentrust['0']['type']==0){
                $data['btc']=$info['0']['btc']-$myentrust['0']['number'];
                $data['dollar']=$info['0']['dollar']+$myentrust['0']['number']*$usd1;
                $update_info->where($condition1) ->save($data);

                $data1['uid']=$this->mid;
                $data1['price']=$usd1;
                $data1['number']=$myentrust['0']['number'];
                $data1['type']=1;
                $data1['state']=1;
                $data1['turnover_time']=time();
                $data1['entrust_time']=$data1['turnover_time'];
                $entrust->data($data1)->add();
            }
            if($myentrust['0']['type']==1){
                $data['btc']=$info['0']['btc']+$myentrust['0']['number'];
                $data['dollar']=$info['0']['dollar']-$myentrust['0']['number']*$usd1;
                $update_info->where($condition1) ->save($data);

                $data1['uid']=$this->mid;
                $data1['price']=$usd1;
                $data1['number']=$myentrust['0']['number'];
                $data1['type']=0;
                $data1['state']=1;
                $data1['turnover_time']=time();
                $data1['entrust_time']=$data1['turnover_time'];
                $entrust->data($data1)->add();
            }
            $this->success("");
        }
        else{ $this->error("");}
    }
}