-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 07 月 25 日 01:47
-- 服务器版本: 5.5.24-log
-- PHP 版本: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `thinksns_3_0`
--

-- --------------------------------------------------------

--
-- 表的结构 `ts_bitcoin_com_entrust`
--

CREATE TABLE IF NOT EXISTS `ts_bitcoin_com_entrust` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `price` double NOT NULL,
  `number` double NOT NULL,
  `state` tinyint(4) NOT NULL,
  `entrust_time` int(11) NOT NULL,
  `turnover_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- 转存表中的数据 `ts_bitcoin_com_entrust`
--

-- --------------------------------------------------------

--
-- 表的结构 `ts_bitcoin_com_info`
--

CREATE TABLE IF NOT EXISTS `ts_bitcoin_com_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `btc` double NOT NULL,
  `dollar` double NOT NULL DEFAULT '500000',
  `f_btc` double NOT NULL,
  `f_dollar` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `ts_bitcoin_com_info`
--
-- --------------------------------------------------------

--
-- 表的结构 `ts_bitcoin_entrust`
--

CREATE TABLE IF NOT EXISTS `ts_bitcoin_entrust` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `price` double NOT NULL,
  `number` double NOT NULL,
  `state` tinyint(4) NOT NULL,
  `entrust_time` int(11) NOT NULL,
  `turnover_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=209 ;

--
-- 转存表中的数据 `ts_bitcoin_entrust`
--

-- --------------------------------------------------------

--
-- 表的结构 `ts_bitcoin_info`
--

CREATE TABLE IF NOT EXISTS `ts_bitcoin_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `btc` double NOT NULL,
  `dollar` double NOT NULL DEFAULT '500000',
  `f_btc` double NOT NULL,
  `f_dollar` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `ts_bitcoin_info`
--
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
