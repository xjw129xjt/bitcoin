<?php
if (!defined('SITE_PATH')) exit();

$db_prefix = C('DB_PREFIX');

$sql = array(
    // Blog数据
    "DROP TABLE IF EXISTS `{$db_prefix}bitcoin_info`;",
    "DROP TABLE IF EXISTS `{$db_prefix}bitcoin_entrust`;",
    "DROP TABLE IF EXISTS `{$db_prefix}bitcoin_com_entrust`;",
    "DROP TABLE IF EXISTS `{$db_prefix}bitcoin_com_info`;",
    // ts_system_data数据
    //"DELETE FROM `{$db_prefix}system_data` WHERE `list` = 'weiba'",
    // 积分规则
   );

foreach ($sql as $v)
    M('')->execute($v);